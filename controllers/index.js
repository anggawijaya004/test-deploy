const { Computer } = require('../models')

function viewAllData(req, res) {
    Computer.findAll()
        .then(computers => {
            // res.status(200).json(computers)
            // res.send('masuk view')
            res.render("computer", { computers })
        })
        .catch(err => {
            console.log(err)
        })
}

function showFormAdd(req, res) {
    res.render("addComputer")
}

function createData(req, res) {
    const { motherboard, os, processor, harddisk, memory } = req.body

    Computer.create({ motherboard, os, processor, harddisk, memory, user_id: 1 })
        .then(computer => {
            // res.status(200).json(computer)
            // res.send('masuk view')
            res.redirect('/')
        })
        .catch(err => {
            console.log(err)
        })
}

function showFormUpdate(req, res) {
    const {id} = req.params
    
    Computer.findOne({where:{id:id}})
    .then(computer => {
        // res.status(200).json(computers)
        // res.send('masuk view')
        res.render("updateComputer", {computer})
    })
    .catch(err => {
        console.log(err)
    })
   
}

function updateData(req, res) {
    const { motherboard, os, processor, harddisk, memory } = req.body
    const { id } = req.params
    Computer.update({ motherboard, os, processor, harddisk, memory, user_id: 1 }, { where: { id: id } })
        .then(computer => {
            // res.status(200).json(computer)
            // res.send('masuk view')
            res.redirect('/')
        })
        .catch(err => {
            console.log(err)
        })
}

function deleteData(req, res) {
    const { id } = req.params
    Computer.destroy({ where: { id: id } })
        .then(computer => {
            // res.status(200).json(computer)
            // res.send('masuk view')
            res.redirect('/')
        })
        .catch(err => {
            console.log(err)
        })
}



module.exports = { viewAllData, createData, showFormAdd, updateData, showFormUpdate, deleteData }