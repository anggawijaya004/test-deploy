const { User } = require('../models')
// const passport = require('../lib/passport')

function format(user) {
    const { id, username } = user
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}


function register(req, res, next) {
    // Kita panggil static method register yang sudah kita buat tadi
    User.register(req.body)
        .then(() => {
            res.redirect('/login')
        })
        .catch(err => next(err))
}

function login(req, res) {
    User.authenticate(req.body)
        .then(user => {
            res.json(
                format(user)
            )
        })
        .catch(err => {
            console.log(err)
        })
}

function whoami(req, res) {
    const currentUser = req.user;
    res.json(currentUser)
}





module.exports = { register, login, whoami }