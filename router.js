const router = require('express').Router()
const {viewAllData, showFormAdd, showFormUpdate, createData, updateData, deleteData} = require('./controllers')
const {register, login, whoami} = require('./controllers/user')
const restrict = require('./middlewares/restrict')

//router of table computers
router.get('/', viewAllData)
router.get('/add', showFormAdd)
router.post('/add', createData)
router.get('/update/:id', showFormUpdate)
router.post('/update/:id', updateData)
router.get('/delete/:id', deleteData)

//auth
router.get('/register', (req, res) => res.render('register'))
router.post('/register', register)
router.get('/login', (req, res) => res.render('login'))
router.get('/whoami', restrict, whoami)
router.post('/login', login)

module.exports = router