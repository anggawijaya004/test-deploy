const express = require('express')
const app = express()
const router = require('./router')
const passport = require('./lib/passport')
const bodyParser = require("body-parser");
const port =  3000

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(passport.initialize())

app.use(router)
app.use(express.json())
app.set("view engine", "ejs")


app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})